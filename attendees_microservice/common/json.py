from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            # if o is a datetime
            return o.isoformat()
            # return the datetime(o) into isoformat so its JSON serializable
        else:
            return super().default(o)


class ModelEncoder(QuerySetEncoder, DateEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            diction = {}
            if hasattr(o, "get_api_url"):
                diction["href"] = o.get_api_url()
            for property in self.properties:
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                diction[property] = value
            diction.update(self.get_extra_data(o))
            return diction
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}
