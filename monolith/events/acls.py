import requests
from .keys import PEXELS_API_KEY


def get_photo(city, state):
    # Create a dictionary for the headers to use in the rquest
    headers = {"Authorization": PEXELS_API_KEY}
    # Create the URL for the request with the city and state
    query = f"{city} {state}"
    url = f"https://api.pexels.com/v1/search?query={query}"
    # Make the rquest
    response = requests.get(url, headers=headers)
    # Parse the JSON response
    picture_url = response.json()["photos"][0]["src"]["original"]
    return {"picture_url": picture_url}


# def get_weather_data(city, state):
